# Task 4.3 Exercises
# computegrade.py
# Program to grade a score between 0.0 and 1.0 using the function computegrade.
# Tim Wang
# 7 Mar 2019

error = 0

def computegrade(x):
    if float(x) >= 1.0:
        print("Bad Score")
    elif float(x) >= 0.9:
        print("A")
    elif float(x) >= 0.8:
        print("B")
    elif float(x) >= 0.7:
        print("C")
    elif float(x) >= 0.6:
        print("D")
    elif float(x) < 0.6:
        print("F")
    return

score = input ("Enter your score: ")
try:
    is_string = float(score)
except:
    error = -1
if error == -1:
    print("Bad Score")
else:
    computegrade(score)


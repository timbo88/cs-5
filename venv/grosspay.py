# Task 3.3 Exercises
# grosspaywithot.py
# Program to calculate the gross pay from the hours and rate per hour entered
# with overtime penalty of 1.5 times.
# Tim Wang
# 5 Mar 2019

hours = input ("Enter hours: ")
try:
    is_string = int(hours)
except:
    print("Error, please enter numeric input")
rate = input ("Enter rate: ")
pay = float(hours) * float(rate)
print ("Pay: %.2f" % round(pay, 2))
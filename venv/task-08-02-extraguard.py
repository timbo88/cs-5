# Task 08-02
# task-08-02-extraguard.py
# Program that extracts the day of the email from the text file mbox-short.txt.
# Added a guardian statement that the From line contains more than three words.
# Tim Wang
# 13 March 2019

fhand = open('mbox-short.txt')
count = 0
for line in fhand:
    words = line.split()
    # print 'Debug:', words
    if len(words) < 3 : continue
    if words[0] != 'From' : continue
    print(words[2])
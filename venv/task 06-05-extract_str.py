# Task 06-05 Exercise
# extract_str.py
# Program that takes extracts a string from a string.
# Tim Wang
# 11 March 2019

str = 'X-DSPAM-Confidence:0.8475'
colonpos = str.find(':')
#print (colonpos)
#commapos = str.find(" ' ",colonpos)
str = str[colonpos+1:]
#print(type(str))
float_str = float(str)
print(float_str)
#print(type(float_str))
# Task 3.3 Exercises
# grosspaywithot.py
# Program to calculate the gross pay from the hours and rate per hour entered
# with overtime penalty of 1.5 times.
# Also checks if hours and rate are entered as digits.
# Tim Wang
# 5 Mar 2019

error = 0
penalty = 1.5
hours = input ("Enter hours: ")
try:
    is_string = int(hours)
except:
    error = -1
if error == -1:
    print("Error, please enter numeric input")
else:
    rate = input ("Enter rate: ")
    try:
        is_string = int(rate)
    except:
        error = -1
    if error == -1:
        print("Error, please enter numeric input")
    else:
        pay = float(hours) * float(rate)
        if float(hours) > 40:
            pay = pay * penalty
        print ("Pay: %.2f" % round(pay, 2))
# Task 09-05
# task-09-05-schoolcount.py
# Program records the domain name (instead of the address) where the message was sent from instead of who the mail
# came from (i.e., the whole email address). At the end of the program, print out the contents of your dictionary.
# Tim Wang
# 14 March 2019

counts = dict()

# Test file is mbox-short.txt
filen = input('Enter file: ')
fhand = open(filen)

for line in fhand:
    words = line.split()
    if (len(words) > 3) and (words[0] == 'From'):
        k = words[1]
        atpos = k.find('@')
        k = k[atpos+1:]
        if k not in counts:
            counts[k] = 1
        else:
            counts[k] += 1
print(counts)

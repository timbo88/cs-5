# Task 3.3 Exercises
# grosspaywithot.py
# Program to calculate the gross pay from the hours and rate per hour entered
# with overtime penalty of 1.5 times if more than 40 hours worked.
# Tim Wang
# 5 Mar 2019


penalty = 1.5
hours = input ("Enter hours: ")
rate = input ("Enter rate: ")
pay = float(hours) * float(rate)
if float(hours) > 40:
    pay = 40.0 * float(rate)
    pay = pay + ((float(hours) - 40) * float(rate) * penalty)
print ("Pay: %.2f" % round(pay, 2))

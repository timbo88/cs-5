# Task 05-05 Exercise
# max-min.py
# Program that prompts for a list of numbers as above and at the end prints out both the
# maximum and minimum of the numbers.
# Tim Wang
# 11 March 2019

smallest = None
largest = None
print('Smallest Before:', smallest)
print('Largest Before:', largest)

# keep looping until input = done

while True:
    line = input('Enter a number: ')
    # input = done, exit while statement
    if line == 'done':
        break

    try:
        is_string = int(line)
        line = float(line)
    #        print ("data type of line --->",type(line))
    #        print ("data type of is_string --->",type(is_string))
    except:
        print("Invalid input")
        continue

    if smallest is None or line < smallest:
        smallest = line
        print('Smallest Loop:', line, smallest)

    if largest is None or line > largest:
        largest = line
        print('Largest Loop:', line, largest)

#    print("After tests Line is", line)

print('Smallest:', smallest)
print('Largest:', largest)



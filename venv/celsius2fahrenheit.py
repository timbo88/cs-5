# Task 2.3 Exercises
# celsius2fahrenheit.py
# Program to convert celsius to fahrenheit.
# Tim Wang
# 5 Mar 2019

celsius = input("Enter temperature in Celsius: ")
fahrenheit = 9/5 * float(celsius) + 32.0
print ("%.1f" % float(celsius), "degrees Celsius is equal to %.1f" % fahrenheit, "degreees fahrenheit")
# Task 3.3 Exercises
# grosspaywithot.py
# Program to take two parameters (hour and rate) and calculate the pay.
# Time and a half overtime applies when 40+ hours are worked.
# Tim Wang
# 6 Mar 2019

error = 0

def computepay(hours, rate):
    pay = float(hours) * float(rate)
    if float(hours) > 40:
        pay = 40.0 * float(rate)
        pay = pay + (float(hours) - 40) * float(rate) * 1.5
    return pay

#if float(hours) > 40:
#   pay = 40.0 * float(rate)
#   pay = pay + ((float(hours) - 40) * float(rate) * penalty)

hrs = input ("Enter hours: ")
try:
    is_string = int(hrs)
except:
    error = -1
if error == -1:
    print("Error, please enter numeric input")
else:
    rph = input ("Enter rate: ")
    try:
        is_string = int(rph)
    except:
        error = -1
    if error == -1:
        print("Error, please enter numeric input")
    else:
        x = computepay(hrs, rph)
        print ("Pay: %.2f" % round(x, 2))


# Task 05-05 Exercise
# average.py
# Program which repeatedly reads numbers until the user enters "done". Once "done"
# is entered, print out the total, count, and average of the numbers. If the user
# enters anything other than a number, detect their mistake using try and except and
# print an error message and skip to the next number.
# Tim Wang
# 8 March 2019


total = 0
count = 0
error = 0
# keep looping until input = done

while True:
    line = input('Enter a number: ')

    # input = done, exit while statement
    if line == 'done':
        break

    try:
        is_string = int(line)
    except:
        print("Invalid input")
        continue

    count = count + 1
    total = total + int(line)

print('Total=', total, 'Count=', count, 'Average=', total/count)

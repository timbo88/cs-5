# Task 08-03
# task-08-03-combinedifs.py
# Program that extracts the day of the email from the text file mbox-short.txt.
# Combined guardian statements to a single if.
# Tim Wang
# 13 March 2019

fhand = open('mbox-short.txt')
count = 0
for line in fhand:
    words = line.split()

    if (len(words) < 3) or (words[0] != 'From') :
#        print('Debug len(words):', len(words), "words[0]", words[0])
        continue
    print(words[2])

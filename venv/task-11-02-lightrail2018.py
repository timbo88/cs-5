# Task 11-02
# task-11-02-lightrail2018.py
# Program to extract the number of Senior/Pensioners who use their Opal cards at the Fish Market Light Rail stop in 2018
# and place the data in a text file "Fish Market Senior 2018.txt".
# Tim Wang
# 16 March 2019

wordtotal = ""
print(type(wordtotal))

#fhand = open("Light Rail Card Type.txt")
fileinput = open('Light Rail Card Type.txt', 'r')
fileoutput = open('Fish Market Senior 2018.txt', 'w')

for line in fileinput:
    words = line.rstrip().split()
#    print(words)
    if words[0] == 'Fish' and words[4] == "Senior/Pensioner":
        wordtotal = words[0]+" "+words[1]
        n = 23
        while n < 35:
            wordtotal = wordtotal+" "+words[n]
            n += 1
#        print(wordtotal)
#        print(type(wordtotal))
        fileoutput.write(wordtotal)

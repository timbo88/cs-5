# Task 08-06
# task-08-06-max-min.py
# Program that prompts for a list of numbers as above and at the end prints out both the
# maximum and minimum of the numbers using the function max() and min().
# Tim Wang
# 13 March 2019

numlist = []

# keep looping until input = done
while True:
    line = input('Enter a number: ')
    # input = done, exit while statement
    if line == 'done':
        break
    try:
        is_string = int(line)
        line = float(line)
    except:
        print("Invalid input")
        continue
    numlist.append(line)

print('Maximum:', max(numlist))
print('Minimum:', min(numlist))

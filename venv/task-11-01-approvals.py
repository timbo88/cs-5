# Task 11-01
# task-11-01-approvals.py
# Program to read each record and output the total number of approvals in the Central district for year 2016
# Tim Wang
# 16 March 2019

apr2016 = 0

fhand = open("DPEapprovals.txt")

for line in fhand:
    words = line.rstrip().split('|')
#    print(words)
    if words[1] == "Central":
        print(words)
        apr2016 = apr2016 + int(words[2])
print(apr2016)

# Task 09-03
# task-09-03-emailcount.py
# Program to read through a mail log, build a histogram using a dictionary to count how many messages have
# come from each email address, and print the dictionary.
# Tim Wang
# 14 March 2019

counts = dict()

# Test file is mbox-short.txt
filen = input('Enter file: ')
fhand = open(filen)

for line in fhand:
    words = line.split()
    if (len(words) > 3) and (words[0] == 'From'):
        k = words[1]
        if k not in counts:
            counts[k] = 1
        else:
            counts[k] += 1

print(counts)




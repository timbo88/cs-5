# Task 06-01 Exercise
# reverse_str.py
# Program that takes a string and prints it out in reverse order.
# Tim Wang
# 11 March 2019

string = input ("Please enter a string: ")
n = len(string) - 1
while n >=0:
    print (string[n])
    n -= 1

# Task 09-01
# task-09-01-storekeys.py
# Program that reads the words in words.txt and stores them as keys in a dictionary.
# It doesn't matter what the values are. Then you can use the in operator as a fast way to check whether
# a string is in the dictionary.
# Tim Wang
# 14 March 2019

worddict = dict()

filen = input('Enter file: ')
fhand = open(filen)

for line in fhand:
    words = line.split()
    for word in words:
        if word not in worddict:
            worddict[word] = ""

print("Worddict:", worddict)




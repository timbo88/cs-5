# Task 08-05
# task-08-05-emails.py
# Program to read through the mail box data and when you find line that starts with "From",
# you will split the line into words using the split function.
# We are interested in who sent the message, which is the second word on the From line.
# Tim Wang
# 13 March 2019

filen = input('Enter file: ')
fhand = open(filen)

for line in fhand:
    words = line.split()
    # print 'Debug:', words
    if len(words) < 3 : continue
    if words[0] != 'From' : continue
    print(words[1])


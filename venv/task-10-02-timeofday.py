# Task 10-02
# task-10-02-timeofday.py
# Program counts the distribution of the hour of the day for each of the messages.
# Test file is mbox-short.txt
# Tim Wang
# 15 March 2019

counts = dict()

filen = input('Enter file: ')
fhand = open(filen)

for line in fhand:
    words = line.split()
    if (len(words) > 3) and (words[0] == 'From'):
        k = words[5]
        k = k[:2]
        counts[k] = counts.get(k, 0) + 1

tmp = list()
for count in counts:
    tmp.append((count, counts[count]))

tmp = sorted(tmp)

for count, counts[count] in tmp:
    print(count, counts[count])
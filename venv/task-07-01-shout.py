# Task 07-01
# task-07-01-shout.py
# Program that reads through a file and print the contents of the file (line by line) all in upper case.
# Tim Wang
# 12 March 2019

file = input("Enter a file name: ")
fhand = open(file)
for line in fhand:
    line = line.rstrip()
    line = line.upper()
    print(line)

# Task 09-04
# task-09-04-mostemail.py
# Program to read through a mail log, build a histogram using a dictionary to count how many messages have
# come from each email address, and print the email with the most emails.
# Tim Wang
# 14 March 2019

counts = dict()

# Test file is mbox-short.txt
filen = input('Enter file: ')
fhand = open(filen)

for line in fhand:
    words = line.split()
    if (len(words) > 3) and (words[0] == 'From'):
        k = words[1]
        if k not in counts:
            counts[k] = 1
        else:
            counts[k] += 1

tmp = list()
for count in counts:
    tmp.append((counts[count],count))

tmp = sorted(tmp, reverse=True)

for counts[count], count in tmp[:1]:
    print(count, counts[count])


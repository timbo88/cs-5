# Task 4.3 Exercises
# grosspaywithot.py
# Program to grade a score between 0.0 and 1.0.
# Tim Wang
# 5 Mar 2019

error = 0
score = input ("Enter your score: ")
try:
    is_string = float(score)
except:
    error = -1
if error == -1:
    print("Bad Score")
else:
    if float(score) >= 1.0:
        print("Bad Score")
    elif float(score) >= 0.9:
        print("A")
    elif float(score) >= 0.8:
        print("B")
    elif float(score) >= 0.7:
        print("C")
    elif float(score) >= 0.6:
        print("D")
    elif float(score) < 0.6:
        print("F")


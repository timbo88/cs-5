# Task 08-04
# task-08-04-romeo.py
# Program to open the file romeo.txt and read it line by line.
# For each line, split the line into a list of words using the split function.
# For each word, check to see if the word is already in a list. If the word is not in the list, add it to the list.
# When the program completes, sort and print the resulting words in alphabetical order.
# Tim Wang
# 13 March 2019

wordlist = []

filen = input('Enter file: ')
fhand = open(filen)

for line in fhand:
    words = line.split()
    print (words)
    for word in words:
        if word not in wordlist:
            wordlist.append(word)

wordlist.sort()
print("Wordlist:", wordlist)#    for words in



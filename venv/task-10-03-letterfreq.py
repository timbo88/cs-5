# Task 10-03
# task-10-03-letterfreq.py
# Program that reads a file and prints the letters in decreasing order of frequency.
# Your program should convert all the input to lower case and only count the letters a-z.
# Your program should not count spaces, digits, punctuation, or anything other than the letters a-z.
# Tim Wang
# 15 March 2019

count = 0
totalcount = 0
wordbank = ""
lettercount = {'a':0, 'b':0, 'c':0, 'd':0, 'e':0, 'f':0, 'g':0, 'h':0, 'i':0, 'j':0, 'k':0, 'l':0, 'm':0, 'n':0,
               'o':0, 'p':0, 'q':0, 'r':0, 's':0, 't':0, 'u':0, 'v':0, 'w':0, 'x':0, 'y':0, 'z':0}

filen = input('Enter file: ')
fhand = open(filen)
#fhand = open("test.txt")

for line in fhand:
    words = line.rstrip().lower()
    wordbank = wordbank + words
#    print(wordbank)

print("Letter  |  No  |  %")
for letter in wordbank:
#    print(letter)
    if letter in lettercount:
        lettercount[letter] = lettercount.get(letter, 0) + 1
        totalcount += 1

for letter in lettercount:
    print((letter, lettercount[letter]), round(lettercount[letter]/totalcount*100,2))

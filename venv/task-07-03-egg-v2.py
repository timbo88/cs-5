# Task 07-03
# task-07-03-egg.py
# Program that reads through a file and calculates the number of subject lines.
# Easter Egg added for input "na na boo boo"
# Tim Wang
# 12 March 2019
# 13 March 2019 - version 2

fname = input('Enter the file name: ')

if fname == "na na boo boo":
    print("NA NA BOO BOO TO YOU - You have been punk'd!")
    exit()

try:
    fhand = open(fname)
except:
    print('File cannot be opened:', fname)
    exit()

count = 0
for line in fhand:
    if line.startswith('X-DSPAM-Confidence:'):
        count = count + 1

print("There were", count, "subject lines in", fname )

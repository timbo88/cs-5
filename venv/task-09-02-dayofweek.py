# Task 09-02
# task-09-02-dayofweek.py
# Program that categorizes each mail message by which day of the week the commit was done with a running count.
# Tim Wang
# 14 March 2019

counts = dict()

# Test file is mbox-short.txt
filen = input('Enter file: ')
fhand = open(filen)

for line in fhand:
    words = line.split()
    if (len(words) > 3) and (words[0] == 'From'):
        k = words[2]
        if k not in counts:
            counts[k] = 1
        else:
            counts[k] += 1

print(counts)




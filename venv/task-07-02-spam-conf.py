# Task 07-02
# task-07-02-spam_conf.py
# Program that reads through a file and calculates the spam confidence.
# Tim Wang
# 12 March 2019

fname = input('Enter the file name: ')
fhand = open(fname)
count = 0
line_total = 0
for line in fhand:
    if line.startswith('X-DSPAM-Confidence:'):
        colonpos = line.find(':')
        line = line[colonpos+1:]
        float_line = float(line)
        line_total = line_total + float_line
#        print(line)
        count = count + 1
asc = line_total / count
print("Average spam confidence: %.12f" % asc)

# Task 08-01
# task-08-01-functions.py
# Program that contains a function called chop that takes a list and modifies it, removing
# the first and last elements, and returns None.
# Then write a function called middle that takes a list and returns a new list that contains
# all but the first and last elements.
# Tim Wang
# 12 March 2019


list1 = ['1','2','3','4','5']
list2 = ['1','2','3','4','5']

def chop(chopped):
    del chopped[0]
    del chopped[-1]

def middle(a):
    middled = a[1:-1]
    return middled

chop(list1)
print(list1)

b = middle(list2)
print("List with no first or last element:", b)
